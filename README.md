# sc3d

A LADSPA plugin for giving 'depth' (or reverb) effect to stereo sound. A mod of the SC4 compressor of SWH collection of LADSPA plugins ([THANKS A LOT, S. W. HARRIS !](https://github.com/swh/ladspa)).

Dowload and  extract the SWH collection of LADSPA plugins from https://github.com/swh/ladspa/

Download the Makefile.diff, POTFILE.diff and sc3d_2567.xml to home folder.

Copy sc3d_2567 to the SWH LADSPA source directory.

Patch the Makefile and po/POTFILES.in using following commmands:

<pre>
<br>
patch Makefile < /home/user/Makefile.diff
patch po/POTFILES.in < /home/user/POTFILES.diff
</pre>
<br>
Build the SWH plugins using instructions in REAME.

Copy the sc3d_2567.so from .libs directory to /usr/lib/ladspa.

<h2>Configuring ALSA</h2>

Insert the following lines in the '.asoundrc' file in home directory.
<br>
If there is already LADSPA plugins in .asoundrc, only copy the lines after <code>plugins</code> keyword to the section of asoundrc with LADSPA configuration.

<pre>
pcm.lspa {
        type ladspa
        slave.pcm "to_output"
        path "/usr/lib/ladspa"
	channels 2
        plugins {
                0 {
                label sc3d
                input {
			controls [ 0 6 1.0121458 200 750 -64 38 0.7 ]
                }
                }
                1 {
                label sc4
                input {
                        controls [ 1 180 350 -60 1.01 20 1 ]
                        controls [ 1 4 1.009999 30 300 -64 38 0.7 ]
                }
                }
        }
}
</pre>
<br>

<h2>Configuring Pulseaudio</h2>

Change the pulseaudio config file /etc/pulse/default.pa to include following lines (replace master=output with whatever is the output sink name) :

<pre>
<br>
load-module module-ladspa-sink sink_name=ladspa_output.sc3d1 label=sc3d plugin=sc3d_2567 master=output control=1,4,1.009846,30,300,-64,38,0.65
load-module module-ladspa-sink sink_name=ladspa_output.sc3d label=sc3d plugin=sc3d_2567 master=ladspa_output.sc3d1 control=0,6,1.011122346,200,750,-64,38,0.65

...

set-default-sink ladspa_output.sc3d
</pre>

Restart pulseaudio and, if all things are OK, the stereo output will have a 'depth' effect to it on any music / sounds that are played.

The Pulseaudio volume control should have 2 "Ladspa Plugin" settings as in screenshot below
<br>
<br>
![Screenshot](screenshot.jpg)


<h2>Configuring Pipewire</h2>

Running the plugin on pipewire requires pipewire-pulse package to be installed.

Run the following commands in correct order.
<pre>
<br>

pactl load-module module-ladspa-sink sink_name=sc3d1 label=sc3d plugin=sc3d_2567 master=combine control=1,4,1.009999,30,300,-64,38,0.65

pactl load-module module-ladspa-sink sink_name=sc3d label=sc3d plugin=sc3d_2567 master=sc3d1 control=0,6,1.011122346,200,750,-64,38,0.65

pactl set-default-sink sc3d

</pre>

<br>
Change the <pre>master=combine</pre> 'combine' text to whatever audio output pipewire is configured.
<br>
That's it. Whatever audio that is played will have the SC3D plugin running.

<hr>
<h2>Parameters</h2>

Running analyseplugin on '/usr/lib/ladspa/sc3d_2567.so' will give the parameters for the plugin.
<hr>
RMS/peak
<br>
The balance between the RMS and peak envelope followers.
<hr>
Number of samples
<br>
The number of samples after which gain is applied.
<br>
Sample number should be about 3 to 10 tops.
<hr>
<b>Ratio (1:n)</b>
<br>
<b>VERY IMPORTANT !</b> Calculates the gain multiplier by formula = (1 - r) / r
<br>
The ratio should be close to 1.0 for better results.<br>
Example ratio value is 1.0121458, which sets the gain multiplier to 0.012000049795<br>
Example of a very high value is 1.0526316, which makes the multiplier 0.05<br>
Example of a low value is 1.009082 (multiplier 0.009)<br>
A high ratio value causes higher 'compressor' effect. To keep this effect low, the ratio should be close to 1.0<br>
<hr>
Attack time
<br>
The attack time in milliseconds.
<hr>
Release time (ms)
<br>
<hr>
The release time in milliseconds.
<hr>
Threshold level (dB)
<br>
The point at which the compressor will start to kick in.
<hr>
Knee radius (dB)
<br>
The distance from the threshold where the knee curve starts.
<hr>
Makeup gain (dB)
<br>
Controls the gain of the makeup input signal in dB's.
<hr>